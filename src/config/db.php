<?php

class db {
  public $con = NULL;

  private $Host = 'mysql';
  private $User = 'root';
  private $Pass = 'root';
  private $Name = 'bdElTiempo';

  function __construct() {
    $this->conectDB();
  }

  public function conectDB() {

    try {
      $options = array(PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ, PDO::ATTR_ERRMODE => PDO::ERRMODE_WARNING);
      $mysqlConnect = "mysql:host=$this->Host;dbname=$this->Name";
      $dbConnecion = new PDO($mysqlConnect, $this->User, $this->Pass, $options);
      $this->con = $dbConnecion;
    } catch (PDOException $e) {

      $data['data'] =  NULL;
      $data['error'] = true;
      $data['msj'] = $e->getMessage();
      
    }

  }
}
