<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app = new \Slim\App;

$app->get('/', function (Request $request, Response $response) {
  echo 'use como ejemplo la ruta:  ';
  echo 'https://miapplicacion.com/eltiempo/public/productos';
  
});


// Obtener todos los productos
$app->get('/productos', function (Request $request, Response $response) {
  $sql = "SELECT * FROM productos";
  try {
    $bd = new db();
    $res = $bd->con->query($sql);
    $productos = $res->fetchAll();

    $data = array();
    if (count($productos)  > 0) {
      $data['data'] =  $productos;
      $data['error'] = false;
      $data['msj'] = 'Datos encontrados';
    } else {
      $data['data'] =  $productos;
      $data['error'] = false;
      $data['msj'] = 'No se encontraron datos';
    }
  } catch (PDOException $e) {
    $data['data'] =  NULL;
    $data['error'] = true;
    $data['msj'] = $e->getMessage();
  }
  echo json_encode($data);
});

// Obtener un producto según su DD
$app->get('/productos/{id}', function (Request $request, Response $response) {
  $id = $request->getAttribute('id');
  $sql = "SELECT * FROM productos where id = $id";
  try {
    $bd = new db();
    $res = $bd->con->query($sql);
    $productos = $res->fetchAll();

    $data = array();
    if (count($productos)  > 0) {
      $data['data'] =  $productos;
      $data['error'] = false;
      $data['msj'] = 'Datos encontrados';
    } else {
      $data['data'] =  $productos;
      $data['error'] = false;
      $data['msj'] = 'No se encontraron datos';
    }
  } catch (PDOException $e) {
    $data['data'] =  NULL;
    $data['error'] = true;
    $data['msj'] = $e->getMessage();
  }
  echo json_encode($data);
});


// Crear un producto
$app->post('/productos/add', function (Request $request, Response $response) {
  $producto = $request->getParam('producto');
  $cantidad = $request->getParam('cantidad');
  
  if( is_numeric( $cantidad ) ){
    $sql = "INSERT INTO productos
              (
              producto,
              cantidad)
              VALUES
              (
              '$producto',
              '$cantidad');
              ";
    try {
      $bd = new db();
      $res = $bd->con->exec($sql);

      $data = array();
      if ( $res ) {
        $data['data'] = $bd->con->lastInsertId();
        $data['error'] = false;
        $data['msj'] = 'Producto guardado';
      } else {
        $data['data'] =  NULL;
        $data['error'] = false;
        $data['msj'] = 'No se encontraron datos';
      }
    } catch (PDOException $e) {
      $data['data'] =  NULL;
      $data['error'] = true;
      $data['msj'] = $e->getMessage();
    }
  }else{
    $data['data'] =  NULL;
    $data['error'] = true;
    $data['msj'] = 'La cantidad debe ser un valor númerico';
  }

  echo json_encode($data);
});


// PUT Modificar cliente 
$app->put('/productos/update/{id}', function (Request $request, Response $response) {
  $id = $request->getAttribute('id');
  $producto = $request->getParam('producto');
  $cantidad = $request->getParam('cantidad');

  if( !isset($id) && is_numeric( $id ) ){
    $data['data'] =  NULL;
    $data['error'] = true;
    $data['msj'] = '';
  }elseif( is_numeric( $cantidad ) ){
    $sql = "UPDATE productos
                SET
                producto = '$producto',
                cantidad = '$cantidad'
                WHERE id = $id;
                ";
    try {
      $bd = new db();
      $res = $bd->con->exec($sql);

      $data = array();
      if ( $res ) {
        $data['data'] = $id;
        $data['error'] = false;
        $data['msj'] = 'Producto actualizado';
      } else {
        $data['data'] =  $id;
        $data['error'] = false;
        $data['msj'] = 'No se actualizaron los datos';
      }
    } catch (PDOException $e) {
      $data['data'] =  NULL;
      $data['error'] = true;
      $data['msj'] = $e->getMessage();
    }
  }else{
    $data['data'] =  NULL;
    $data['error'] = true;
    $data['msj'] = 'La cantidad debe ser un valor númerico';
  }

  echo json_encode($data);
});


// Borrar producto
$app->delete('/productos/delete/{id}', function (Request $request, Response $response) {
  $id = $request->getAttribute('id');
  
  if( is_numeric( $id ) ){
    $sql = "DELETE FROM productos WHERE id = $id;";
    try {
      $bd = new db();
      $res = $bd->con->exec($sql);

      $data = array();
      if ( $res ) {
        $data['data'] = $id;
        $data['error'] = false;
        $data['msj'] = 'Producto borrado correctamente';
      } else {
        $data['data'] =  $id;
        $data['error'] = false;
        $data['msj'] = 'No se pudo borrar el producto o ya no existe en la base de datos';
      }
    } catch (PDOException $e) {
      $data['data'] =  NULL;
      $data['error'] = true;
      $data['msj'] = $e->getMessage();
    }
  }else{
    $data['data'] =  NULL;
    $data['error'] = true;
    $data['msj'] = 'El id debe ser nmerico';
  }

  echo json_encode($data);
});
