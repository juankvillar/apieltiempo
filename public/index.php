<?php
header("Access-Control-Allow-Origin: *");

$env = 'desarrollo';
if( $env ){
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);
}

require '../vendor/autoload.php';
require '../src/config/db.php';

// Rutas prudcutos
require '../src/routes/productos.php';

$app->run();